package com.teste.piorfilme.core.repository;

import java.util.List;
import java.util.Optional;

import com.teste.piorfilme.core.entity.Movie;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {
	Optional<Movie> findByYearAndTitleAndStudiosAndProducersAndWinner(Integer year, String title, String studios, String producers, String winner);
	List<Movie> findByWinner(String winner);
	List<Movie> findByWinnerOrderByYearAsc(String winer);
}