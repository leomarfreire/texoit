package com.teste.piorfilme.core.service;

import com.teste.piorfilme.core.integration.response.IntervaloPremioResponse;

import org.springframework.stereotype.Component;

@Component
public interface IMovieService {
	IntervaloPremioResponse getIntervaloDePremio();
}