package com.teste.piorfilme.core.service;

import java.util.List;
import java.util.stream.Collectors;

import com.teste.piorfilme.core.entity.Movie;
import com.teste.piorfilme.core.integration.response.IntervaloPremioResponse;
import com.teste.piorfilme.core.integration.response.PremioResponse;
import com.teste.piorfilme.core.repository.MovieRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MovieService implements IMovieService {
	@Autowired
	private MovieRepository movieRepository;

	@Override
	public IntervaloPremioResponse getIntervaloDePremio() {
		IntervaloPremioResponse response = new IntervaloPremioResponse();

		List<Movie> movieList = movieRepository.findByWinnerOrderByYearAsc("yes");

		if (movieList != null && !movieList.isEmpty()) {
			Integer min = 9999;
			Integer max = 1;

			for (Movie movie : movieList) {
				List<Movie> movieByCurrentProducers = movieList.stream().filter(m -> m.getProducers().equals(movie.getProducers())).collect(Collectors.toList());
				
				if (movieByCurrentProducers != null && !movieByCurrentProducers.isEmpty() && movieByCurrentProducers.size() > 1) {
					Integer yearMoveTemp = movieByCurrentProducers.get(0).getYear();

					for (Movie currentMovie : movieByCurrentProducers) {
						if ((currentMovie.getYear() - yearMoveTemp < min) && (currentMovie.getYear() - yearMoveTemp != 0)) {
							min = currentMovie.getYear() - yearMoveTemp;
						}

						if (currentMovie.getYear() - yearMoveTemp > max) {
							max = currentMovie.getYear() - yearMoveTemp;
						}

						yearMoveTemp = currentMovie.getYear();
					}
				}
			}

			for (Movie movie : movieList) {
				List<Movie> movieByCurrentProducers = movieList.stream().filter(m -> m.getProducers().equals(movie.getProducers())).collect(Collectors.toList());
				
				if (movieByCurrentProducers != null && !movieByCurrentProducers.isEmpty() && movieByCurrentProducers.size() > 1) {
					Integer reviousWin = movieByCurrentProducers.get(0).getYear();

					int i = 0;
					for (Movie currentMovie : movieByCurrentProducers) {
						if (i > 0) {
							if (currentMovie.getYear() - reviousWin == min && this.notIncluded(response.getMin(), currentMovie)) {
								response.getMin().add(this.createPremioResponse(currentMovie.getProducers(), min, reviousWin, currentMovie.getYear()));
							} else if (currentMovie.getYear() - reviousWin == max && this.notIncluded(response.getMax(), currentMovie)) {
								response.getMax().add(this.createPremioResponse(currentMovie.getProducers(), max, reviousWin, currentMovie.getYear()));
							}
						} 
						i++;
						reviousWin = currentMovie.getYear();
					}
				}
			}
		}

		return response;
	}

	private boolean notIncluded(List<PremioResponse> list, Movie currentMovie) {
		for (PremioResponse premioResponse : list) {
			if (premioResponse.getProducer().equals(currentMovie.getProducers()) && premioResponse.getFollowingWin().equals(currentMovie.getYear())) {
				return Boolean.FALSE;
			}
		}

		return Boolean.TRUE;
	}

	private PremioResponse createPremioResponse(String producer, Integer interval, Integer reviousWin, Integer followingWin) {
		PremioResponse premioResponse = new PremioResponse();
		premioResponse.setProducer(producer);
		premioResponse.setInterval(interval);
		premioResponse.setPreviousWin(reviousWin);
		premioResponse.setFollowingWin(followingWin);

		return premioResponse;
	}
}