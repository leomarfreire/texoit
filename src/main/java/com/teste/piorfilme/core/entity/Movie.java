package com.teste.piorfilme.core.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TB_MOVIE")
@SequenceGenerator(name = "sqMovie", sequenceName = "SQ_MOVIE", allocationSize = 1)
public class Movie implements Serializable {
	private static final long serialVersionUID = 3280432584550120827L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sqMovie")
	private Long id;

	@Column(name = "YEAR", nullable = false)
	private Integer year;

	@Column(name = "TITLE", nullable = false)
	private String title;

	@Column(name = "STUDIOS", nullable = false)
	private String studios;

	@Column(name = "PRODUCERS", nullable = false)
	private String producers;

	@Column(name = "WINNER", nullable = true)
	private String winner;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getStudios() {
		return studios;
	}

	public void setStudios(String studios) {
		this.studios = studios;
	}

	public String getProducers() {
		return producers;
	}

	public void setProducers(String producers) {
		this.producers = producers;
	}

	public String getWinner() {
		return winner;
	}

	public void setWinner(String winner) {
		this.winner = winner;
	}
}