package com.teste.piorfilme.core.integration.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class IntervaloPremioResponse implements Serializable {
	private static final long serialVersionUID = -8860804720860052987L;

	private List<PremioResponse> min = new ArrayList<>();
	private List<PremioResponse> max = new ArrayList<>();

	public List<PremioResponse> getMin() {
		return min;
	}

	public void setMin(List<PremioResponse> min) {
		this.min = min;
	}

	public List<PremioResponse> getMax() {
		return max;
	}

	public void setMax(List<PremioResponse> max) {
		this.max = max;
	}
}
