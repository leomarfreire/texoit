package com.teste.piorfilme.config;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.servers.Server;

@Configuration
@SecurityScheme(
	name = "Authorization",
	scheme = "apiKey",
	type = SecuritySchemeType.APIKEY,
	in = SecuritySchemeIn.HEADER
)
public class OpenApiConfig {
	@Value("${api.url}")
	private String apiUrl;

	private static final String API_KEY = "Authorization Key";

	@Bean
	public OpenAPI customCofiguration() {
		return new OpenAPI()
			.components(new Components())
			.addServersItem(new Server().url(apiUrl))
			.info(this.customInfo())
			.security(Collections.singletonList(new SecurityRequirement().addList(API_KEY)));
	}

	private Info customInfo() {
		return new Info()
			.title("Pior Filme")
			.description("API para possibilitar a leitura da lista de indicados e vencedores da categoria Pior Filme do Golden Raspberry Awards.")
			.version("v1")
			/* .termsOfService(apiTerms)
			.contact(this.customContact())
			.license(this.customLicense()) */;
	}

	/* private Contact customContact() {
		return new Contact().name(apiContactName).url(apiContactUrl).email(apiContactEmail);
	}

	private License customLicense() {
		return new License().name(apiLicenseName).url(apiLicenseUrl);
	} */
}