package com.teste.piorfilme.config;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.teste.piorfilme.core.entity.Movie;
import com.teste.piorfilme.core.repository.MovieRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DbInit {
	@Autowired
	private MovieRepository movieRepository;

	@PostConstruct
	private void postConstruct() {
		try {
			Reader reader = Files.newBufferedReader(Paths.get("csv\\movielist.csv"));
			CSVParser parser = new CSVParserBuilder()
								.withSeparator(';')
								.withIgnoreQuotations(true)
								.build();
			CSVReader csvReader = new CSVReaderBuilder(reader)
									.withSkipLines(1)
									.withCSVParser(parser)
									.build();
			List<String[]> lines = csvReader.readAll();

			for (String[] column : lines) {
				Movie movie = new Movie();
				movie.setYear(Integer.valueOf(column[0].trim()));
				movie.setTitle(column[1].trim());
				movie.setStudios(column[2].trim());
				movie.setProducers(column[3].trim());
				movie.setWinner(column[4].trim());

				Optional<Movie> currentMovie = movieRepository.findByYearAndTitleAndStudiosAndProducersAndWinner(
					Integer.valueOf(column[0].trim()),
					column[1].trim(),
					column[2].trim(),
					column[3].trim(),
					column[4].trim()
				);

				if (currentMovie.isEmpty()) {
					movieRepository.save(movie);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}