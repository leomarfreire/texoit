package com.teste.piorfilme.controller;

import javax.servlet.http.HttpServletResponse;

import com.teste.piorfilme.core.integration.response.IntervaloPremioResponse;
import com.teste.piorfilme.core.service.IMovieService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping(value = "v1.0/teste")
@CrossOrigin(origins = "*")
@Tag(name = "Teste", description = "Controle Teste")
public class AuthorizerController {
	@Autowired
	private IMovieService movieService;

	@Operation(
		summary = "Versão da Aplicação",
		description = "Retorna a versão da aplicação"
	)
	@GetMapping(
		value = "/intervalo-de-premio",
		produces = "application/json"
	)
	public IntervaloPremioResponse getIntervaloDePremio(HttpServletResponse serveletResponse) {
		return movieService.getIntervaloDePremio();
	}
}