### Requisitos

- Autor: [Leomar Carvalho](https://www.linkedin.com/in/leomarcarvalho/)
- Este projeto foi desenvolvido com o intuíto de servir de avaliação da TEXOIT.COM

### Requisitos

- Versão do Java 1.8
- Maven

### Configurando

Para carregar outro CSV, será preciso subistituir o arquivo que consta na pasta `csv\`;

Em seguida, deverá ser configurado o arquivo de propriedades `application.properties` conforme modelo abaixo:

    server.port=8080
	api.url=http://localhost:${server.port}
	
	# DATASOURCE
	spring.datasource.url=jdbc:h2:file:./data/piorfilme
	spring.datasource.driverClassName=org.h2.Driver
	spring.datasource.username=sa
	spring.datasource.password=123456
    
	# H2 Console
	spring.h2.console.enabled=true
	spring.h2.console.path=/h2-console
	
	# JPA
	spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
	spring.jpa.hibernate.ddl-auto=update

### Exemplo da API

A API foi desenvolvida utilizando SpringBoot, JPA, DataBase H2, OpenAPI e Swagger.

![](https://gitlab.com/leomarfreire/texoit/-/raw/master/openApi.png)
